# AAPT NBN Portal Service Qualification

URL Endpoint: https://connect.boomi.com/ws/simple/getServiceQualification <br>
Authentication: Basic (Credentials stored as metadata in Salesforce)

Service Qualification is performed in two steps: <br>
1. Address Search - Finding the Location ID based on the information supplied by the Google API
2. Service Qualification - Finding whether the location is NBN-serviceable based on the Location ID found in the previous step




## Address Search

### Request

The initial address data is generated when the customer selects the appropriate location on Google Maps. The customer can optionally add additional data 
(floor, unit number, etc...) and then initiate the search for NBN-serviceable addresses that fit the criteria. 

The request payload for this type of search should be

```javascript
{
    "requestType": "addressLookup",
    "serviceProvider": [
		<<`list of providers`>>
	],
    "address": {
		<<`address details`>>
	}
}
```

The element  `requestType` should have the value `addressLookup`. Example:

```json
{
    "requestType": "addressLookup",
    "serviceProvider": [
		"nbn"
    ],
    "address": {
        "addressInformation": "",
        "subAddressType": "",
        "subAddressTypeNumber": "",
        "streetNumber": "50",
        "streetNumberSuffix": "",
        "streetName": "Appel",
        "streetType": "Street",
        "streetSuffix": "",
        "suburb": "Graceville",
        "state": "QLD",
        "postcode": "4075"
    }
}
```

### Response

There are three possible outcomes to this request
* No addresses found - the element `status` has value `no address` to indicate that no addresses were found.

```json
{
    "responseType": "addressLookup",
	"status" : "no addresses",
    "error" : {
        "errorMessage" : "No NBN for this address"
    }
}

```

* Found address(es) - in this case, `addresses` list will contain details for all available addresses. Example:
```json
{
  "responseType" : "addressLookup",
  "status" : "success",
	"addresses" : [
    {
      "Id" : "LOC000050844791",
      "displayAddress" : "GATEWAY UNIT 25 50 APPEL STREET SURFERS PARADISE QLD 4217",
      "locationId" : "LOC000050844791",
      "subAddressType" : "UNIT",
      "subAddressNumber" : "25",
      "streetNumber" : "50",
      "streetName" : "APPEL",
      "streetType" : "STREET",
      "suburb" : "SURFERS PARADISE",
      "state" : "QLD",
      "postcode" : "4217"
    },
    {
      "Id" : "LOC000051047770",
      "displayAddress" : "GATEWAY UNIT 7 50 APPEL STREET SURFERS PARADISE QLD 4217",
      "locationId" : "LOC000051047770",
      "subAddressType" : "UNIT",
      "subAddressNumber" : "7",
      "streetNumber" : "50",
      "streetName" : "APPEL",
      "streetType" : "STREET",
      "suburb" : "SURFERS PARADISE",
      "state" : "QLD",
      "postcode" : "4217"
    }
	]
}
```

More details are found in the next section.

**Note:** *Upon selecting the appropriate address, a new API call is made to Boomi to qualify the service.*

* Server Error - `errorMessage` and (optionally) `additionalErrorMessage` are populated with the corresponding error messages from the AAPT's fault response. These are not user-friendly!
```javascript
{
  "responseType" : "addressLookup",
  "status" : "server error",
  "error" : {
      "errorMessage" : << error-specific text >>,
      "additionalErrorMessage" : << error-specific text >>
  }
}
```

Example:
```javascript
{
    "responseType" : "error",
    "error" : {
        "errorMessage" : "cvc-complex-type.2.4.b: The content of element 'ns2:address' is not complete. One of '{\"http://www.aapt.com.au/FrontierLink/xsd\":state}' is expected.",
        "additionalErrorMessage" : "org.xml.sax.SAXParseException; cvc-complex-type.2.4.b: The content of element 'ns2:address' is not complete. One of '{\"http://www.aapt.com.au/FrontierLink/xsd\":state}' is expected."
    }
}
```

### Address Search - Summary

Ignoring server errors, the two possible outcomes are given below.

```mermaid
graph LR;
  id0["Choose approximate address on the Map"]-->id1["Address Lookup"];
  id1-->|"no address"|id2["Return Fail Result"];
  id1-->|"success"|id3["Return List of Addresses"];
  style id1 stroke-width:5px
```

## Service Qualification

### Request

To initiate the SQ, a similar request is made as with Address Search, but now the element `requestType` has the value `serviceQualification` and is followed by the list of location Ids and selected products for each. Example:
site

```javascript
{
    "requestType": "serviceQualification",
    "addresses": [
		{
			"uniqueId" : "LOC123456789",
			"products" : [
				"nbn",
				"telstra-fibre"
			]
		}	
	]
}
```

### Response

If NBN is unavailable for the given location, an error response will be returned containing the `errorMessage`  and optionally `additionalErrorMessage` which should be presented
to the customer. 

Successful response contains the list of available products for the site (at the moment, only AAPT NBN NWB):

```javascript
{
  "responseType": "success",
  "productAvailabilityList": [<< list of available products with their relevant configurations >>]
}
```
Also, the `responseType` has the value `sucess`. For NBN, the response has the following information.

```javascript
{
  "responseType": "success",
  "productAvailabilityList": [
    {
      "Id": "NBN",
      "product": "NBN",
      "details": {
        "technology": << technology name >>,
        "nrc": << nrc of the tail service calculated based on the table given below>>,
        "availableSpeeds": [<< list of available speeds >>]
      }
    }
  ]
}
```

Example of a successful response:

```javascript
 {
  "responseType": "serviceQualification",
  "status" : "multiple addresses",
  "productAvailabilityList" : [
	  {
		  "uniqueId" : <<loc id (for now)>>,
		  "availableProducts" : [
			  {
		      "Id": "nbn-vpn",
		      "product": "nbn-vpn",
		      "details": {
			        "technology": "FTTN",
			        "nrc": "0.00",
			        "availableSpeeds": [
			            "up to 12/1Mb",
			            "up to 25/5Mb"
		        	]
				}
			  },
			  {
		      "Id": "nbn-internet",
		      "product": "nbn-internet",
		      "details": {
			        "technology": "FTTN",
			        "nrc": "0.00",
			        "availableSpeeds": [
			            "up to 12/1Mb",
			            "up to 25/5Mb"
		        	]
				}
			  }
		  ]
	  }
  ]
 }
```

### Speeds

Available speeds' list obtained from AAPT's SQ response must be validated against those speeds GCOMM offers as we do not offer every speed AAPT does. The table below lists
the speeds that are offered by GCOMM as well as their names in AAPT and GCOMM system


| AAPT | GCOMM |
| ------ | ------ |
| 12Mbps/1Mbps | up to 12/1Mb |
| 25Mbps/5Mbps | up to 25/5Mb | 
| 25Mbps/10Mbps | up to 25/10Mb | 
| 50Mbps/20Mbps | up to 50/20Mb | 
| 100Mbps/40Mbps | up to 100/40Mb | 



### NRC Calculation for NBN Tail Service

The NRC is a combination of the following three 

| Charge | Description | Cost |
| ------ | ------ | ------ |
| Subsequent Installation Charge | Applicable only for copper NBNs and applies in case the service must be ordered on a new or used CPI| $300 |
| New Developments Charge | Applicable for all types, location-specific | $273 | 
| Professional Installation | Applicable only for FTTC | $150 |

The final tail NRC is the sum of all applicable charges.

## Sequence Diagram

The interaction of the four key agents is given in the follwing sequence diagram.

```mermaid
sequenceDiagram
    participant C as Customer
    participant P as Portal
    participant B as Boomi
    participant A as AAPT
    C->>P: select address
    P->>B: pass address details
    B->>A: findServiceProviderLocationId Request
    A->>B: findServiceProviderLocationId Response
    alt no address
    B->>P: notify
    P->>C: notify
    else multiple addresses
    B->>P: return the addresses' table
    P->>C: return the addresses' table
    C->>P: select the appropriate address
    P->>B: qualify for specified address
    B->>A: qualifyProduct Request
    A->>B: qualifyProduct Response    
    else single address
    B->>A: qualifyProduct Request
    A->>B: qualifyProduct Response
    end
```

